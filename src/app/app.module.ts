import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { baseURL } from '../shared/baseurl';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';


import { HttpClientModule} from '@angular/common/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ProcessHttpmsgProvider } from '../providers/process-httpmsg/process-httpmsg';

import { Covid19Provider } from '../providers/covid19/covid19';
import { CountrydetailPage } from '../pages/countrydetail/countrydetail';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { Push} from '@ionic-native/push';
import { environment } from '../environments/environments';

import { AngularFireDatabase, AngularFireDatabaseModule } from '@angular/fire/database';
import { SocialSharing } from '@ionic-native/social-sharing';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CountrydetailPage
  ],
  imports: [
   
    Ng2SearchPipeModule,
    HttpClientModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  
    CountrydetailPage
  ],
  providers: [
    SocialSharing,
    Covid19Provider,
    StatusBar,
    Push,
    AngularFireDatabase,

    SplashScreen,
    { provide: 'BaseURL', useValue: baseURL },
   
    {provide: ErrorHandler, useClass: IonicErrorHandler},

    ProcessHttpmsgProvider,

  
 


  ]
})
export class AppModule {}
