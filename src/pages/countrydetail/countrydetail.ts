import { Component, Inject } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Country } from '../../shared/country';
import { ThrowStmt } from '@angular/compiler';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the CountrydetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-countrydetail',
  templateUrl: 'countrydetail.html',
})
export class CountrydetailPage {
country : Country;

date: any;
time : any;
today : any;
message: any="The App Shows covid-19 cases in Realtime , check it out using this link";
link : any="linkedin.com/in/abdulkadir-abdullahi-4859a0183/";
subject: any="Stay Safe";
file: any=null;

  constructor(private socialSharing: SocialSharing, public navCtrl: NavController, public navParams: NavParams, @Inject('BaseURL') private BaseURL) {
    this.country = navParams.get('country');
    this.date=new Date().toDateString();
    this.today=new Date();
     this.time = this.today.getHours() + ":" + this.today.getMinutes() + ":" + this.today.getSeconds();
     }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountrydetailPage');
  }

  sendShare() {
    this.socialSharing.share(this.message,this.subject,this.file,this.link);
  }  

}
