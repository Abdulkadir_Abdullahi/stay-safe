import { Component,OnInit, Inject, NgModule } from '@angular/core';
import { Covid19Provider } from '../../providers/covid19/covid19';
import { CountrydetailPage } from '../countrydetail/countrydetail';
import { NavController, NavParams} from 'ionic-angular';
import { Country } from '../../shared/country';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import 'firebase/database';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  countries:   Country[];
  loadedcountries:   Country[];
  continents: any = null;
  nigerias: Object[];
  name : any= "Abdulkadir Abdullahi";
  email : any= "mailto:Abdulkadirfs71@gmail.com";
  whatsapp : any= "https://wa.me/2348141398600";
  info: any = null;
  searchCountry: any;
  errMess : any;
  memoryinfo : any = null;
  date : any;
  today: any;
  time: any;
  staysafe ;
  message: any="The App Shows covid-19 cases in Realtime , check it out using this link";
  link : any="linkedin.com/in/abdulkadir-abdullahi-4859a0183/";
  subject: any="Stay Safe";
  file: any=null;


  constructor(  private socialSharing: SocialSharing, public db: AngularFireDatabase, private covidService: Covid19Provider,public navCtrl: NavController, public navParams: NavParams,@Inject('BaseURL') private BaseURL) { 
    
    this.covidService.getAll().subscribe((data)=>{
      this.info = data;
    });

    this.covidService.getContinents().subscribe((data)=>{
      this.continents = data;
    });

    this.date=new Date().toLocaleDateString();
    this.getDatafromDatabase();
    this.today=new Date();
    this.time = this.today.getHours() + ":" + this.today.getMinutes() + ":" + this.today.getSeconds();

  }
  


ngOnInit() {
    this.covidService.getCountries()
      .subscribe(countries => this.countries = countries,
        errmess => this.errMess = <any>errmess),
        this.loadedcountries=this.countries;    
}
  
sendShare() {
    this.socialSharing.share(this.message,this.subject,this.file,this.link);
}  

getDatafromDatabase(){
    this.db.object('/staysafe').valueChanges().subscribe(
    data => {
      this.staysafe=data;
    })
}


countrySelected(event, country) {
    this.navCtrl.push(CountrydetailPage, {
      country: country
    });
    }

}
