import {Countryin} from "./countryin"
export interface Country {
country: String;
countryInfo : Countryin[];
cases:number;
todayCases:number;
deaths:number;
todayDeaths:number;
recovered:number;
active:number;
critical:number;
casesPerOneMillion:number,
deathsPerOneMillion:number;


}