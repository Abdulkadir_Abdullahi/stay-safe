import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { baseURL } from '../../shared/baseurl';
import { ProcessHttpmsgProvider } from '../process-httpmsg/process-httpmsg';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import { Country } from '../../shared/country';

/*
  Generated class for the Covid19Provider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Covid19Provider {

  
   apiUrl = "https://corona.lmao.ninja/v2/"
  apiUrl3 = "https://corona.lmao.ninja/v2/"

  constructor(private http: HttpClient, private processHTTPMsgService: ProcessHttpmsgProvider) { }

  getAll() {
    return this.http.get(`${this.apiUrl}all`, );
  }

  getCountries(): Observable<Country[]> {
    return this.http.get(baseURL + 'countries')
              .map(res => { return this.processHTTPMsgService.extractData(res); })
              .catch(error => { return this.processHTTPMsgService.handleError(error); });
    }
  

  getContinents(){
    return this.http.get(`${this.apiUrl3}continents`);
  }

 
  
  
}
